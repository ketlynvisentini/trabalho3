package trabalho3.visual;

import trabalho3.Cod.VendaCod;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class FXMLControllerVendasProntasTela implements Initializable {

    @FXML
    private TableView<VendaCod> tabelaVenda;
    @FXML
    private TableColumn<VendaCod, Integer> idVendaCol;
    @FXML
    private TableColumn<VendaCod, Date> dataVendaCol;
    @FXML
    private TableColumn<VendaCod, String> pagamentoCol;
    @FXML
    private TableColumn<VendaCod, String> vendedorCol;
    @FXML
    private TableColumn<VendaCod, String> clienteCol;
    private ObservableList<VendaCod> venda;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        venda = tabelaVenda.getItems();
        idVendaCol.setCellValueFactory(new PropertyValueFactory<>("id_Venda"));
        dataVendaCol.setCellValueFactory(new PropertyValueFactory<>("dataVenda"));
        pagamentoCol.setCellValueFactory(new PropertyValueFactory<>("pagamento"));
        vendedorCol.setCellValueFactory(new PropertyValueFactory<>("vendedor"));
        clienteCol.setCellValueFactory(new PropertyValueFactory<>("id_Cliente"));
        ArrayList<VendaCod> al = VendaCod.getAll();
        for(VendaCod v : al){
            venda.add(v);
        }      
    }    

    @FXML
    private void Retorno(ActionEvent event)  throws Exception {
        replaceScene("FXMLdocumentVendaTela.fxml", event);
    }
    
      public void replaceScene(String fxml, Event event) throws Exception {
        Parent blah = FXMLLoader.load(getClass().getResource(fxml));
        Scene scene = new Scene(blah);
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(scene);
        appStage.show();
    }
}
