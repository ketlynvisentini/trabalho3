package trabalho3.visual;

import trabalho3.Cod.ClienteCod;
import trabalho3.Cod.ConexaoCod;
import trabalho3.Cod.EscreveArqCod;
import trabalho3.Cod.ItensCod;
import trabalho3.Cod.FrutasCod;
import trabalho3.Cod.TipoFrutaCod;
import trabalho3.Cod.VendaCod;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.NotSerializableException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class FXMLControllerVendaTela implements Initializable {

    @FXML
    private TextField vendedorTF;
    @FXML
    private DatePicker dataVendaDP;
    @FXML
    private ChoiceBox<String> pagamentoCB;
    @FXML
    private Label aviso;
    @FXML
    private ChoiceBox<ClienteCod> clienteCB;
    @FXML
    private TableView<ItensCod> tabela;
    @FXML
    private ChoiceBox<TipoFrutaCod> produtoCB;
    @FXML
    private TextField quantidadeTF;
    @FXML
    private TableColumn<TipoFrutaCod, String> produtoCol;
    @FXML
    private TableColumn<ItensCod, Integer> quantidadeCol;

    private ObservableList<ItensCod> itensVenda;
    @FXML
    private TableColumn<FrutasCod, String> saborCol;
    @FXML
    private TableColumn<FrutasCod, Float> precoCol;
    @FXML
    private TableColumn<ItensCod, Float> precoTotalCol;
    @FXML
    private ChoiceBox<FrutasCod> saborCB;
    @FXML
    private Label estoqueLabel;

    private VendaCod venda = new VendaCod();
    @FXML
    private Button atualiza;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        atualiza.setVisible(false);
        pagamentoCB.getItems().add("Dinheiro");
        pagamentoCB.getItems().add("Cartão");
        pagamentoCB.getItems().add("Boleto");

        for (ClienteCod t : ClienteCod.getAll()) {
            clienteCB.getItems().add(t);
        }

        itensVenda = tabela.getItems();
        produtoCol.setCellValueFactory(new PropertyValueFactory<>("produto"));
        quantidadeCol.setCellValueFactory(new PropertyValueFactory<>("quantidade"));
        saborCol.setCellValueFactory(new PropertyValueFactory<>("sabor"));
        precoCol.setCellValueFactory(new PropertyValueFactory<>("valor"));
        precoTotalCol.setCellValueFactory(new PropertyValueFactory<>("precoTotal"));
        this.tabela.setItems(itensVenda);
        itensVenda.clear();
        ArrayList<TipoFrutaCod> al = TipoFrutaCod.getAll();
        for (int x = 0; x < al.size(); x++) {
            produtoCB.getItems().add(al.get(x));
        }

        produtoCB.valueProperty().addListener(new javafx.beans.value.ChangeListener<TipoFrutaCod>() {
            @Override
            public void changed(ObservableValue<? extends TipoFrutaCod> observable, TipoFrutaCod oldValue, TipoFrutaCod newValue) {
                atualizaSabor(newValue);
            }

        });

        saborCB.valueProperty().addListener(new javafx.beans.value.ChangeListener<FrutasCod>() {
            @Override
            public void changed(ObservableValue<? extends FrutasCod> observable, FrutasCod oldValue, FrutasCod newValue) {
                if (newValue == null) {
                    return;
                }
                estoqueLabel.setText("" + newValue.getEstoque());
            }

        });
        carregarCarrinho();
    }

    @FXML
    private void voltar(ActionEvent event) throws Exception {
        replaceScene("FXMLDocumentPrincipal.fxml", event);
    }

    @FXML
    private void confirmar(ActionEvent event) {
        if ((vendedorTF.getText().isEmpty() || pagamentoCB.getSelectionModel().isEmpty() || clienteCB.getSelectionModel().isEmpty() || dataVendaDP.getValue() == null)) {
            aviso.setText("Preencha todos os campos...");
        } else {
            aviso.setText("Cadastro");
            preencheVenda();
            venda.inserirVenda(venda);
            aviso.setText("Cadastrado com sucesso");
            String log = " Cliente " + venda.getId_Cliente() + " comprou: ";
            boolean flag = true;
            for (ItensCod iv : itensVenda) {
                if (flag) {
                    flag = false;
                } else {
                    log += ", ";
                }
                log += iv.getQuantidade() + " " + iv.getId_Fruta().getTipo().getNome() + " de " + iv.getId_Fruta().getSabor();
                iv.setId_Venda(venda);
                iv.inserirItensCod(iv);

            }
            log += " do(a) vendedor(a) " + venda.getVendedor() + " através de " + venda.getPagamento() + " na data " + venda.getDataVenda();
            EscreveArqCod.createLog("logs.txt", log);

            pagamentoCB.getSelectionModel().clearSelection();
            vendedorTF.clear();
            clienteCB.getSelectionModel().clearSelection();
            dataVendaDP.getEditor().clear();
            itensVenda.clear();
            File file = new File ("venda.Ket");
            file.delete();
        }
    }

    public void replaceScene(String fxml, Event event) throws Exception {
        Parent blah = FXMLLoader.load(getClass().getResource(fxml));
        Scene scene = new Scene(blah);
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(scene);
        appStage.show();
    }

    private void carregarCarrinho() {
        try {
            FileInputStream fout = new FileInputStream("venda.ket");
            ObjectInputStream oos = new ObjectInputStream(fout);
            List<ItensCod> lista = (ArrayList<ItensCod>) oos.readObject();
            for(ItensCod iv : lista){
                itensVenda.add(iv);
            }
            oos.close();
            fout.close();
        } catch (EOFException | NotSerializableException | FileNotFoundException ex) {
            salvarCarrinho();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void preencheVenda() {
        venda.setPagamento(pagamentoCB.getSelectionModel().getSelectedItem());
        venda.setVendedor(String.valueOf(vendedorTF.getText()));
        venda.setId_Cliente(clienteCB.getSelectionModel().getSelectedItem());
        venda.setDataVenda(Date.valueOf(dataVendaDP.getValue()));
    }

    private void salvarCarrinho() {
        try {
            FileOutputStream fout = new FileOutputStream("venda.Ket");
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            System.out.println(itensVenda);
            oos.writeObject(new ArrayList<ItensCod>(itensVenda));
            oos.close();
            fout.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void adicionaProdutos() {
        ItensCod l = new ItensCod();
        FrutasCod escolhido = saborCB.getSelectionModel().getSelectedItem();
        l.setId_Fruta(escolhido);
        String qtdDigitadaS = quantidadeTF.getText();
        if (qtdDigitadaS.equals("")) {

            aviso.setText("Quantidade digitada incorreta!");
            return;
        }
        int qtdDigitada = Integer.parseInt(qtdDigitadaS);
        if (qtdDigitada <= escolhido.getEstoque()) {
            escolhido.setEstoque(escolhido.getEstoque() - qtdDigitada);
        } else {
            aviso.setText("Quantidade digitada maior que o estoque!");
            return;
        }
        l.setQuantidade(qtdDigitada);
        l.setValor(escolhido.getValor());
        l.setProduto(escolhido.getTipo().getNome());
        l.setSabor(escolhido.getSabor());
        l.setPrecoTotal(escolhido.getValor() * qtdDigitada);
        quantidadeTF.clear();
        produtoCB.getSelectionModel().clearSelection();
        saborCB.getSelectionModel().clearSelection();
        itensVenda.add(l);
        salvarCarrinho();
    }

    private void atualizaSabor(TipoFrutaCod tipo) {
        saborCB.getItems().clear();
        estoqueLabel.setText("");
        if (!produtoCB.getSelectionModel().isEmpty()) {
            String sql = "SELECT * from oo_Frutas where id_tipo = " + tipo.getId_Tipo();
            for (ItensCod iv : itensVenda) {
                String nome = " and sabor <> '";
                nome += iv.getId_Fruta().getSabor() + "'";
                sql += nome;
            }
            ConexaoCod c = new ConexaoCod();
            Connection con = c.getConexao();
            PreparedStatement ps = null;

            try {
                ps = con.prepareStatement(sql);
                ResultSet r = ps.executeQuery(); // executa a string no statement
                while (r.next()) {
                    FrutasCod s = new FrutasCod();
                    s.setId_Fruta(r.getInt("id_Fruta"));
                    s.setValor((float) r.getInt("valor"));
                    s.setTipo(tipo);
                    s.setEstoque(r.getInt("estoque"));
                    s.setSabor(r.getString("sabor"));
                    saborCB.getItems().add(s);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            saborCB.getItems().clear();
        }
    }

    @FXML
    private void verVendas(ActionEvent event) throws Exception {
        replaceScene("FXMLDocumentVendasProntas.fxml", event);
    }

    @FXML
    private void atualizar(ActionEvent event) {
        tabela.getSelectionModel().getSelectedItem().setProduto(produtoCB.getSelectionModel().toString());
        tabela.getSelectionModel().getSelectedItem().setSabor(saborCB.getSelectionModel().toString());
        tabela.getSelectionModel().getSelectedItem().setQuantidade(Integer.parseInt(quantidadeTF.getText()));
        adicionaProdutos();
    }

    @FXML
    private void mostrarAtualizar(MouseEvent event) {
        if (tabela.getSelectionModel().getSelectedItem() != null) {
            atualiza.setVisible(true);
        }
    }
}
