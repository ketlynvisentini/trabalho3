
package trabalho3.visual;
import trabalho3.Cod.EscreveArqCod;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class FXMLControllerTelaPrincipal implements Initializable {

    @Override
    public void initialize(URL url, ResourceBundle rb) { }    

    @FXML
    private void cliente(ActionEvent event) throws Exception {        
         EscreveArqCod.createLog("logs.txt", "Clicou no Cliente");
         replaceScene("FXMLDocumentClienteTela.fxml", event);
    }

    @FXML
    private void sushi(ActionEvent event) throws Exception {
         EscreveArqCod.createLog("logs.txt", "Clicou na fruta");
         replaceScene("FXMLControllerFrutaTela.fxml", event); 
    }

    @FXML
    private void venda(ActionEvent event) throws Exception {
         EscreveArqCod.createLog("logs.txt", "Clicou na Venda");
         replaceScene("FXMLControllerVendaTela.fxml", event);
    }
    
     public void replaceScene(String fxml, Event event) throws Exception {
        Parent blah = FXMLLoader.load(getClass().getResource(fxml));
        Scene scene = new Scene(blah);
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(scene);
        appStage.show();
    }
}

