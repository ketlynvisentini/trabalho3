/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho3.Cod;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

    

public class ItensCod implements Serializable {

  
    private static final long serialVersionUID = 1L;

    private FrutasCod id_Fruta;

    private VendaCod id_Venda;

    private float valor;

    private String sabor;

    private String produto;

    private float precoTotal;

    private int quantidade;

  
    public ItensCod() {
    }

    public void setId_Fruta(FrutasCod id_Fruta) {
        this.id_Fruta = id_Fruta;
    }

    public String getSabor() {
        return sabor;
    }

    public void setSabor(String sabor) {
        this.sabor = sabor;
    }

    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public float getPrecoTotal() {
        return precoTotal;
    }

    public void setPrecoTotal(Float precoTotal) {
        this.precoTotal = precoTotal;
    }

    public FrutasCod getId_Fruta() {
        return this.id_Fruta;
    }

    public void setId_Venda(VendaCod id_Venda) {
        this.id_Venda = id_Venda;
    }

    public VendaCod getId_Venda() {
        return this.id_Venda;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public float getValor() {
        return this.valor;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getQuantidade() {
        return this.quantidade;
    }

    public void inserirItensCod(ItensCod itensCod) {
        ConexaoCod c = new ConexaoCod();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String insertTableSQL = "INSERT INTO itens_venda (id_Fruta,id_venda,valor,quantidade) VALUES (?,?,?,?)";
        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1, id_Fruta.getId_Fruta());
            preparedStatement.setInt(2, id_Venda.getId_Venda());
            preparedStatement.setFloat(3,valor);
            preparedStatement.setInt(4, quantidade);
            preparedStatement.executeUpdate();
            System.out.println("Foi inserido na tabela ItensVenda!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static ArrayList<ItensCod> getAll() {
        ConexaoCod c = new ConexaoCod();
        ArrayList<ItensCod> al = new ArrayList<ItensCod>();
        Connection con = c.getConexao();
        PreparedStatement ps = null;
        String sql = "select *from Itens_Cod";  // pega tudo oq tem dentro
        try {
            ps = con.prepareStatement(sql);
            ResultSet r = ps.executeQuery(); // executa a string no statement
            while (r.next()) { // passa pelo arraylist
                ItensCod l = new ItensCod(); // se existe uma resposta, cria uma nova Venda

                al.add(l);
            }
        } catch (SQLException ex) {
            return null;
        }
        return al;
    }

}


